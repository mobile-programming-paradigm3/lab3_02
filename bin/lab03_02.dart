import 'dart:io';

void main(List<String> arguments) {
  taobin();
}

class taobin {
  taobin() {
    SelectMenu();
    String type = (stdin.readLineSync()!);
    Select(type);

    print("กรุณาเลือกประเภท");
    SelectWaterType();
    String WaterType = (stdin.readLineSync()!);
    ColdHotSmoothie(WaterType);

    SelectSweetnessLevel();
    print("กรุณาเลือกระดับความหวาน");
    String SweetnessLevels = (stdin.readLineSync()!);
    SweetnessLevel(SweetnessLevels);

    SelectStraw();
    print("กรุณาเลือกความต้องการ");
    String Straws = (stdin.readLineSync()!);
    Straw(Straws);

    price();
    print(ans);
  }
}

List ans = [];
var sum = 0;
void SelectMenu() {
  print("กรุณาเลือกประเภทเครื่องดื่ม");
  print("1 : เมนูแนะนำ");
  print("2 : กาแฟ");
  print("3 : ชา");
  print("4 : นม,โกโก้ และคาราเมล");
  print("5 : โปรตีนเชค");
  print("6 : โซดา และอื่นๆ");
}

void RecommendedMenu() {
  print("กรุณาเลือกเมนู");
  print("1 : นมคาราเมล");
  print("2 : โกโก้");
  print("3 : โอริโอ้ปั่นภูเขาไฟ");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("นมคาราเมล");
    sum = 25;
  } else if (menu == "2") {
    ans.add("โกโก้");
    sum = 25;
  } else if (menu == "3") {
    ans.add("โอริโอ้ปั่นภูเขาไฟ");
    sum = 25;
  }
}

void coffee() {
  print("กรุณาเลือกเมนู");
  print("1 : เอสเปรสโซ่");
  print("2 : ลาเต้");
  print("3 : คาปูชิโน่");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("เอสเปรสโซ่");
    sum = 25;
  } else if (menu == "2") {
    ans.add("ลาเต้");
    sum = 25;
  } else if (menu == "3") {
    ans.add("โคาปูชิโน่");
    sum = 25;
  }
}

void tea() {
  print("กรุณาเลือกเมนู");
  print("1 : เก็กฮวย");
  print("2 : ชาขิง");
  print("3 : ชาขิงมะนาว");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("เก็กฮวย");
    sum = 25;
  } else if (menu == "2") {
    ans.add("ชาขิง");
    sum = 25;
  } else if (menu == "3") {
    ans.add("ชาขิงมะนาว");
    sum = 25;
  }
}

void MilkCocoaAndCaramel() {
  print("กรุณาเลือกเมนู");
  print("1 : นม");
  print("2 : นมคาราเมล");
  print("3 : โกโก้");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("นม");
    sum = 25;
  } else if (menu == "2") {
    ans.add("นมคาราเมล");
    sum = 25;
  } else if (menu == "3") {
    ans.add("โกโก้");
    sum = 25;
  }
}

void ProteinShake() {
  print("กรุณาเลือกเมนู");
  print("1 : มัจฉะโปรตีน");
  print("2 : โกโก้โปรตีน");
  print("3 : สตอเบอร์รี่โปรตีน");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("มัจฉะโปรตีน");
    sum = 25;
  } else if (menu == "2") {
    ans.add("โกโก้โปรตีน");
    sum = 25;
  } else if (menu == "3") {
    ans.add("สตอเบอร์รี่โปรตีน");
    sum = 25;
  }
}

void SodaAndOthers() {
  print("กรุณาเลือกเมนู");
  print("1 : แดงมะนาวโซดา");
  print("2 : เต่าทรงพลังโซดา");
  print("3 : สตอเบอร์รี่โซดา");
  String menu = (stdin.readLineSync()!);
  if (menu == "1") {
    ans.add("แดงมะนาวโซดา");
    sum = 25;
  } else if (menu == "2") {
    ans.add("เต่าทรงพลังโซดา");
    sum = 25;
  } else if (menu == "3") {
    ans.add("สตอเบอร์รี่โซดา");
    sum = 25;
  }
}

void Select(String type) {
  if (type == "1") {
    RecommendedMenu();
  } else if (type == "2") {
    coffee();
  } else if (type == "3") {
    tea();
  } else if (type == "4") {
    MilkCocoaAndCaramel();
  } else if (type == "5") {
    ProteinShake();
  } else if (type == "6") {
    SodaAndOthers();
  }
}

void SelectWaterType() {
  print("1 : ร้อน");
  print("2 : เย็น");
  print("3 : ปั่น");
}

void ColdHotSmoothie(String WaterType) {
  if (WaterType == "1") {
    ans.add("ร้อน");
    sum = sum;
  } else if (WaterType == "2") {
    ans.add("เย็น");
    sum = sum + 5;
  } else if (WaterType == "3") {
    ans.add("ปั่น");
    sum = sum + 10;
  }
}

void SelectSweetnessLevel() {
  print("1 : หวานน้อย");
  print("2 : หวานปกติ");
  print("3 : หวานมาก");
}
void SweetnessLevel(String Level) {
  if (Level == "1") {
    ans.add("หวานน้อย");
  } else if (Level == "2") {
    ans.add("หวานปกติ");
  } else if (Level == "3") {
    ans.add("หวานมาก");
  }
}

void SelectStraw() {
  print("1 : รับหลอดรับฝา");
  print("2 : ไม่รับหลอดไม่รับฝา");
  print("3 : ไม่รับอะไรเลย");
}

void Straw(String x) {
  if (x == "1") {
    ans.add("รับหลอดรับฝา");
  }
  if (x == "2") {
    ans.add("ไม่รับหลอดไม่รับฝา");
  }
  if (x == "3") {
    ans.add("ไม่รับอะไรเลย");
  }
}
void price(){
  ans.add("ราคา $sum บาท");
}

